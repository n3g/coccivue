import Vue from 'vue'
import Vuex from 'vuex'
import App from './App.vue'
import router from './router'

Vue.config.productionTip = false

// Fn to get random nb between 2 - used for random x and y
function randomIntFromInterval(min, max) {
  return Math.floor(Math.random() * (max - min + 1) + min)
}

// Fn to generator a random color
function getRandomColor() {
  const r = Math.round(Math.random() * 255)
  const g = Math.round(Math.random() * 255)
  const b = Math.round(Math.random() * 255)
  return `rgb(${r},${g},${b})`
}

Vue.use(Vuex)
const store = new Vuex.Store({
  state: {
    coccinelles: [],
    canvas: {
      width: 800,
      height: 800
    }
  },
  mutations: {
    populate() {
      this.state.coccinelles = []
      for (let i = 0; i < 5; i++) {
        const ballRadius = 50
        this.state.coccinelles.push({
          id: i,
          counter: 0,
          ballRadius: ballRadius,
          x: randomIntFromInterval(
            ballRadius,
            this.state.canvas.width - ballRadius
          ),
          y: randomIntFromInterval(
            ballRadius,
            this.state.canvas.height - ballRadius
          ),
          dx: Math.random(),
          dy: Math.random(),
          color: getRandomColor(),
          isVisible: true
        })
      }
    },
    makeItBounce(state, target) {
      let cocci = state.coccinelles[target]
      cocci.x += cocci.dx
      cocci.y += cocci.dy

      if (
        cocci.x > state.canvas.width - cocci.ballRadius ||
        cocci.x < cocci.ballRadius
      ) {
        cocci.dx = -cocci.dx
      }

      if (
        cocci.y > state.canvas.height - cocci.ballRadius ||
        cocci.y < cocci.ballRadius
      ) {
        cocci.dy = -cocci.dy
      }
    },
    changeCocciColor(state, target) {
      state.coccinelles[target].color = getRandomColor()
    },
    increaseCounter(state, target) {
      state.coccinelles[target].counter++
    }
  }
})

store.commit('populate')

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
