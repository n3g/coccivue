import Vue from 'vue'
import VueRouter from 'vue-router'
import Garden from '../views/Garden.vue'
import CountInfo from '../views/CountInfo.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Garden',
    component: Garden
  },
  {
    path: '/countinfo',
    name: 'CountInfo',
    component: CountInfo
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
